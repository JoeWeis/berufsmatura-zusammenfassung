# Französische Revolution

## Begriffe

### Abstimmung nach Stände

Adel, Klerus, 3. Stand haben gleich viele Stimmen.

### Abstimmung nach Kopf

Die Stimmenanzahl hängt von den Anzahl Personen im jeweiligen Stand ab.

### Volkssouveränität

Innerstaatliche Souveränität, Selbstbestimmung des Volkes. Es gelten:

- **Freiheit:** Bürgerrechte: Meinungsfreiheit, Freiheit der Wahlen und Gewerberecht
- **Gleichheit:** Jeder (ausser Frauen) sollte alles erreichen können, Gleichheit vor dem Gesetz
- **Brüderlichkeit:** Solidarität

### Konstitutionelle Monarchie

Monarchie mit einem Parlament und einer Verfassung.

### Parteien

#### Jakobiner

Wollten die Monarchie mit Gewalt abschaffen, ihre Fans waren v.a. die städtische Unterschicht. Robbespierre war ihr Chef.

#### Girondisten

Stammten aus dem gehobenen Bürgertum und waren für die Abschaffung der Monarchie

#### Feuillants

Wollten an der konstitutionellen Monarchie festhalten. 

## Situation vor der Revolution

1788 herrschte in Frankreich Hungersnot und Armut.  Dies hatte folgende Gründe:

- Niederlage im 7 Jährigen Krieg 
- Ausgaben des König
- Missernte.

Die Bevölkerung war in **drei Stände** aufgeteilt:

- Klerus (Steuerfrei)
- Adlige (Steuerfrei)
- Bürgertum (Reich und Arm)

## Zusammenfassung

Aufgrund der Krise rief König Ludwig XVI die Versammlung der Gerneralstände ein. Der 3. Stand möchte dort die Abstimmung nach Köpfen und nicht nach Ständen durchsetzten.
Um das Ziel zu erreichen machten sich die Bürgerlichen vertreten eigenständig und gründeten mit dem Ballhausschwur die Nationalversammlung. 
Der Widerstand vom Königshaus führte führte zur Radikalisierung der Revolutionsbewegung. 
Als Start dazu wird oft der Sturm der Bastille genant. Die Revolutionäre RevolutionäreBevölkerung werden dabei aus dem Staatsgefängnis befreit.

## Ablauf

### 1788 Hunger und Armut

- König Ludwig XVI ruft die **Generakversammlung** ein. 
- Der 3. Stand möchte das pro Kopf Wahlsystem einführen.

### 1789 Strum auf die Bastille

Mit dem Strum auf die Bastille, das Gefängnis von Frankreich, wurde ein Zeichen zur Revolution gegen den König gesetzt. Im Gefängnis befinden sich viele Revolutionäre.

### 1791 Verfassung

- Der König unterschreibt die Verfassung. 
- Frankreich wurde zur Konstitutionellen Monarchie 
- Die Parteien Feuillants, Girondisten, Jakobiner entstanden.

### 1792-1794 Jakobiner an der Macht

- Gleichzeitig plant deren Sturz in dem er sich mit anderen Ländern zur Allianz verbündet.
- Um die Allianz zu verhindern führen die Jakobiner eine Schreckensherrschaft (Terror System).
- Die Jakobiner köpfen den König und schaffen das Königtum ab. Dies ist eine Kriegserklärung gegen die Allianz
- Das Französische Volk (Freiwillige Soldaten) kämpfen gegen die Allianz
- Die Terrorherrschaft führt zu **Spannung Im Volk,** sodass schliesslich Robespierre
  und andere Jakobiner hingerichtet werden .

### 1795 - 1998 Aufschwung Napoleon

Napoleon gewann fast alle Kriege im Ausland, brachte das **Raubgold und füllte damit die Staatskassen**. Mit den gewonnen Kriegen konnte er die Armut bekämpfen.. Er sorgte militärisch für frieden und ruhe im Land.

### 1799 Ende der Revolution

### 1800 Herrschaft Napoleon

**1802:** Luisiana wird an die USA verkauft um Geld zu verdienen. 

**1804:** Napoleon lässt sich zum **Kaiser krönen** und führt den **Code Civile** (Entspricht dem ZGB) ein.

**1806:** Ganz Mitteleuropa ist unter französischer Herrschaft. Napoleon führt eine Kontinentalsperre gegen Großbritannien einen.

**1812:** Die Franzosen werden an der Bersina Brücke nach einem **kalten Winter von den Russen geschlagen**. Zuvor haben die Russen, als Kriegstaktik ihre eigene **Städten Niedergebrannt**.

**1813:** Die Niederlage bei Leibzig führt zu der Verbannung Napoleons nach Elba.

**1815:** Krieg und Niederlage in Waterloo führen zur zweiten Verbannung, durch den Wienerkongress nach Helena.





