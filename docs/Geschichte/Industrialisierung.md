# Industrialisierung
## Gegenüberstellung
### Vorteile
- Einführung der Uhrzeit -> Arbeiten nach Zeit
- Städte wachsen
- Verkehr wächst
- Banken
- Aktiengesellschaften
- Neuen Ausbildungen entstehen
- Fortschritt in der Medizin
- Eisenerzeugung mir Koks als Brennstoff
###Nachteile
- Tiefe Löhne 
- Mehr Arbeiter als Arbeit
- Umweltprobleme
- Soziale Probleme
- Arbeitslosigkeit
- Seuchen verbreiten sich

##Ablauf
1. Der Anfang bildet die Textilherstellung.
2. Daraus folgte die Chemie und Farbenindustrie
3. Die Lebensmittelindustrie entlastete die Hausfrauen.
4. Banken und Versicherung ermöglichten neue Anschaffungen.
### Nahrungsmittel
- 1879 gründet Rodolphe Lindt Lindt Schokolade in Bern.
- In Vevey beginnt Henri Nestlé mit Babynahrung
### Schuhe
Der Schuhfabrikant Carl Franz Bally gründete 1855 die ersten Krankenkasse für diene Arbeiter
### Uhrenindustrie
In Genf arbeiten 1785 rund 20‘000 Personen in der Uhrenindustrie.
Der Erfolg hat sich in den Jura und nach Neuenburg verbreitet.
###Maschinenindustrie
- Eine der ersten Textilindustrie verkauft seine Maschienen und wurde so zu einem Maschinenproduzenten (Escher, Wyss & Co. Zürich) 
- Die **Kontinentalsperre verhinderte den Import** aus England.
- Die Dampfmaschienen forderten mehr Kohlenabbau.
- Die Schweiz hatte viel Wasserenergie aber wenig Kohle.
- Dafür waren sie ein Pioneer bei der Einführung von elektrischen Getriebe
### Chemie
- Die Chemie Industrie wanderten von Frankreich nach Basel. (C em1sche Industrie in Basel **CIBA**)
- Da es in Frankreich Restriktionen bei der Patentanmeldung gab.
- Wurde die Chemie immer mehr nach Basel ausgelagert.
###Banken und Versicherungen
- Die erste Bank war die Schweizerische Kredrtanstalt (Crédit Suisse). Sie unterstützte vor allem den Einsenbahnbau. 
- Die Gründer dieser Bank gründeten auch die **Schweizerische Lebensversicherung und Rentenanstelt.
- Später wurden Kantonalbanken und Sparkassen wie Zahlreiche andere Banken gegründet.
- Die erste Schweizer Börse öffnete 1850 in Genf.
#### Banknote
- Die erste Banknote wurde 1716 in Paris gedruckt.
- Die Schweiz verfasste ein Gesetz zur einheitlichen Banknote 1905
- 1907 nahm die Nationalbank ihren Betrieb auf.
