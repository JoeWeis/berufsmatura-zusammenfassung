#Bismarck, Otto Eduard Leopold

##Der deutsch-französische Krieg 1870/71

### Vorgeschichte

Es gibt den Deutschen

###Ursache und Anlass

Spanien suchte einen neuen König. Bismark unterstützt zusammen mit König Wilhelm ein vielversprechender Kandidat war Prinz Leopold. Frankreich zeigten sich empört es gab gleichzeitig, gravierende innenpolitische Mängel zu überdecken. Prinz Leopold von Hohenzollern-Sigmaringen zogen daraufhin die Kandidatur zurück. Daraufhin verlangte Gramont (Botschafter) von König Wilhelm folgendes:

- Entschuldigung
- Versprechen, nie wieder einer hohenzollernschen Thronkandidatur in Spanien zuzustimmen.

### Emser Depesche

Bismark erhielt die ein Telegramm in dem Willhelm die Forderungen schieldert. Bismark kürzte dies wie folgt.

*Nachdem die Nachricht von der Entsagung des Erbprinzen von Hohenzollern der Kaiserlich Französischen Regierung von der Königlich Spanischen amtlich mitgeteilt worden sind, hat der Französische Botschafter in Ems an S. Maj. den König noch die Forderung gestellt, ihn zu autorisiren, dass er nach Paris telegraphire, dass S. Maj. der König sich für alle Zukunft verpflichte, niemals wieder seine Zustimmung zu geben, wenn die Hohenzollern auf ihre Kandidatur wieder zurückkommen sollten.* 

*Seine Maj. der König hat es darauf abgelehnt, den Franz. Botschafter nochmals zu empfangen, und demselben durch den Adjutanten vom Dienst sagen lassen, dass S. Majestät dem Botschafter nichts weiter mitzutheilen habe.*

Bismarck veröffentlichte dieser Artikel in der Zeitung. Bismarcks Veränderungen hatten folgenden Auswirkungen.

- Der König darf nie wider die Kandidatur eines Königs in Spanien unterstützen -> Das macht die Deutschen wütend.
- Das Fehlen der Information, das Willhelm nicht mit dem König reden will, weil er noch keine Informationen hat. -> Das macht die Franzosen wütend.