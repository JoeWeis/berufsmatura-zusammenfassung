# Imperialismus

## Definition

> Als Imperialismus wird die Herrschaft eines infolge seiner industriellen Entwicklung weiterentwickelten Staates über weniger entwickelte Länder bezeichnet. Die Epoche des Imperialismus umfasst den Zeitraum zwischen 1880 und 1918. Während dieser Zeit betrieben die europäischen Grossmächte eine Politik des expansiven (sich ausdehnenden) Nationalismus. Imperiale Herrschaft wurde direkt (Besetzung und Einrichtung einer Kolonialregierung) oder indirekt (Kontrolle über die eingesetzte einheimische Regierung) ausgeübt.

Von einem politischen Gleichgewicht in Europa konnte seit der Gründung des Deutschen Reichs 1871 keine Rede mehr sein: Der neue Staat wuchs schnell zum ernsthaften Konkurrenten der anderen europäischen Mächte heran.

Das beunruhigte vor allem Grossbritannien *"Greater Britain"* und Frankreich *"Empire Francais"*, sie hatten sich zum Ziel gesetzt, die eigene Macht zu vergrössern. Aber auch Staaten wie Japan und die USA und das vergleichsweise noch wenig industrialisierte Russland machten seit etwa 1880 die imperialistische Ausdehnung zum Ziel ihrer Aussenpolitik.

Die alten Kolonialmächte nutzten ihre Handelsstützpunkte und Kolonien als Ausgangspunkte für nun viel umfangreichere territoriale Eroberungen. Im Vergleich zum alten Kolonialismus konnte die steigende Wirtschaftskraft sowie die militärische Überlegenheit während des  „Imperialismus" eingesetzt werden. 

### Grossbritannien *"Greater Britain"*

England hat die Kolonien in Amerika durch die Gründung der Vereinigten Staaten verloren. Möchte aber Ihre Machtposition in der Welt aufrechterhalten und weiter ausbauen.

In Ägypten hatte eine Englische Regierung keinen Erfolg. Deshalb besetzten die Engländer politisch einflussreiche Positionen, die aber im Hintergrund tätig waren.

> Um die Stellungen, die den grössten Einfluss ausüben und den geringsten Neid erwecken, sollten sich die Europäer bemühen. [Britische Aussenminister Lord Robert Salisbury 1879]

### Frankreich

Frankreich möchte ihre bisherigen Kolonien in Afrika ausbauen. Ausserdem möchten sie Kolonien in Asien gewinnen. Dabei hoffen sie sich Wirtschaftlichen Wachstum und Erweiterung des Marktes. 

Der ständiger Konkurrenzkampf zwischen Frankreich und England war allgegenwärtig. 

### Vereinigten Staaten

Auch die USA wurde in der Zwischenzeit zu einem Industrialisierten Land. Die Vereinigten Staaten verfügten über viel Land und Ressourcen. Deshalb verfolgten sie die Strategie ihre Absatzmärkte zu erweitern. 
Ihre Kolonien sollten eine eigene Regierung unter dem Amerikanischen Recht bilden. Diese sollten verpflichtet sein eine Handlungsbeziehung mit den Vereinigten Staaten einzugehen.

### Russland

Um die Sicherheit an  grenznahen Gebiete zu gewährleisten werden die umliegenden Gebiete durch Russland militärisch kontrolliert. Daraus entsteht eine stetige Erweiterung des Russischen Herrschaftsgebiet.

## Der Dollarimperialismus

Unter 'Dollarimperialismus' versteht man den *wirtschaftlichen* **Imperialismus der** **USA** und der dortigen Konzerne seit 1900. Das heisst: keine militärische Besetzung der beherrschten Länder, sondern *Ausbeutung* *durch* *wirtschaftliche Vorherrschaft* in den sogenannten **Bananenrepubliken,** kleinere Länder in **Lateinamerika oder Afrika**

Der Höhepunkt des amerikanischen Dollarimperialismus wird 1898, durch den Ausbruch des **spanisch-amerikanischen Krieges** in **Kuba,** das seit 1492 in spanischem Besitz war, erreicht. Amerikanische Grossuntemehmer kontrollierten Kuba bereits wirtschaftlich(Tabak, Zuckerrohr).

### United Fruit Company

Die United Fruit Company besass (und besitzt heute noch) grosse Landflächen in Mittelamerika. United Fruit dominiert(e) durch seine Wirtschaftskraft die kleinen Staaten . 

Die wirtschaftlichen Interessen der Firma waren mehrfach Anlass für das politische Eingreifen der USA in Mittelamerika. Beispielsweise veranlassten sie die Stützung des Präsidenten von Honduras um Steuervorteile zu erhalten.

Ausserdem revolutionierte der Konzern die Handelsschifffahrt, indem er die Entwicklung von Kühlschiffen vorantrieb. Sie erfanden ein Schiff welches Personen und Passagiere transportieren konnten. In den 1930er Jahre war die Schiffsflotte „Great White Fleet" der UFC die grösste der Welt.

Nach zahlreichen Imageverbesserung mit teils umstrittenen Umweltschutzorganisation, änderten sie den Namen zu **Chiquita** Brands International. 