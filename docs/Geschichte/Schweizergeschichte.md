# Schweizergeschichte

## Begriffe

### Mythos

Ein Mythos ist in seiner ursprünglichen Bedeutung eine Erzählung, mit der Menschen und Kulturen ihr Welt- und Selbstverständnis zum Ausdruck bringen. Mythen erheben einen Anspruch auf Geltung für die von Ihnen behauptete Wahrheit. Wichtigste Funktion der Mythen: **Sie schweissen eine Gruppe zusammen**.

### Föderalismus

Das Land ist in **mehreren Stufen unterteilt**. (Gemeinde, Kanton, Staat)
Alle Entscheidungen werden so weit unten wie möglich gefällt.

### Jesuiten

Die Schweiz verbot den Jesuiten in der Schweiz tätig zu sein. So verhinderten die, dass sie an Schulen unterrichten.

#### Merkmale

- Selbstgewählte Armut
- Nicht erkennbare Klosterbrüder
- Erster Studiengang: Theologie
- Zrster Studiengang: Irgendein Lehramt (Mathematik, Physik)

## Staatskonzept

### Willensnation

Die Schweiz existiert, weil **die Schweizer*innen die Schweiz wollen**.
Sprachlich gibt es in der Schweiz hürden. 

- Gesetzesbücher werden in mehreren Sprachen übersetzt. 
- Medien Konferenzen werden immer auf Französisch gehalten.

### Kulturnation

Kulturnation ist ein Staatskonzept der Aufklärer.
Eine Nation wird nach der Kultur eingeteilt. Die **Kulturgrenzen enspricht der Sprachgrenzen**.

# Der Sonderbund

### Seiten

| Konservativ (Katilisch) | Liberal (Freiheit) |
| ----------------------- | ------------------ |
| Freiburg                | Bern               |
| Luzern                  | Zürich             |
| Zug                     | Genf               |
| Schwyz                  | Tessin             |
| Uri                     | Basel              |
| Wallis                  | Glarus             |
| Obwalden                | St.Gallen          |
| Nidwalden               | ...                |

### Ablauf

1. Die **Freischarenzüge** versuchten zwei mal die konservative Regierung des Kantons Luzern zu stürzen und die Jesuiten zu vertreiben. 
2. Zur verteidigen gründeten die **konservativen Kantone den Sonderbund**. Sie hatten verbündete konservative Länder. (Frankreich, Österreich) 
3. Um den Sonderbund auszulösen beschloss die **Tagsatzung** (Regierung der Schweiz) eine Truppe zu mobilisieren (General: Guillaume-Henri Dufour).
4. November 1847 ausbruch des Sonderbundkrieges
5. Die Truppe der Tagsatzung nimmt **zuerst Freiburg ein**.
6. Nach der schlacht in Luzern war **der Krieg für die Liberalen gewonnen.**
7. Schweiz wird zum Bundesstaat und verfasst die Bundesverfassung von 1848

### Fakten

- 150 Tote
- 400 Verwundete
- 25 Tage

## Entstehung Schweiz

### Alte Eidgenossenschaft

Einziges gemeinsames Instrument ist die **Tagsatzung** (Beschlüsse nur bei Einstimmigkeit) jeder Kanton ist ein eigener Staat, 13 alte Orte.

### Helvetische Republik

**Besetzung durch französische Armee CH** wird Teil von Frankreich, Aarau ist Hauptstadt die föderalistische Struktur der schweiz wird völlig eliminiert. Äußerer zeichen der Anlehnung an das französische Vorbild ist die **Einführung einer Trikolore** in den Farben grün rot gelb.

### Meditation

Wieder mehr Autonomie aber Napoleon zwingt der Schweiz eine **föderalistische Verfassung** auf.**Die Mediationsakte** gibt den größten Teil der staatlichen Kompetenzen an die 19 Kantone der neuen Eidgenossenschaft ab. Die Tagsatzung als nicht ständige Konferenz der Kantone wird wieder eingeführt. Einzig der Aussenpolitik ist dem Bund vorbehalten.

### Restauration

Teilweise Wiederherstellung des Ancien Regime. Der Herrscher war nicht eine von Volk in einem souveränen Akt geschaffene Institution (Volkssouveränität ) sondern stand aus eigener unbedingter Autorität der Herrschende über dem Volk. Staatenbund **Rückkehr zur Rechtsungleichheit**. Die neu geschaffenen Kantone Aargau, Thurgau, Waadt und St. Galle. bleiben entgegen den Bestrebungen einiger konservativer Kreise bestehen Zensuswahlsystem.

### Regeneration

Basler Kantonsteilung, Staatenbund, **Kampf um den liberalen Staat**, viele Sympathien in der Bevölkerung für die liberalen Ideen, Kulturkampf, Freischarenzüge, Sonderbundskrieg, Bauernbefreiung

### Bundesstaat 

Bundesstaat (Föderation), BVs 1848/1874, Kulturkampf der Liberalen gegen die (katholische) Kirche, Binnenmarkt.

