# Quadratische Funktionen

## Definition

Eine Funktion mit der Funktionsgleichung $f (x) = ax^2 + bx + c$ heisst quadratische Funktion. Die einfachste quadratische Funktion hat die Funktionsgleichung $y = f(x) = x^2$ . 

## Formeln

Parabeln können in folgenden drei Formen ausgedrückt werden: 
Grundform Scheitelform1 Nullstellenform2 

| Name | Formel |
| --------- | -------------- |
| Grundform | $y=ax^2 +bx+c$ |
| Scheitelform | $y = a(x + b)^2 + c$ |
| Nullstellenform | $y = a(x + p)(x + q)$ |

## Eigenschaften der Parabel

Die Parabelgleichung $y = a(x − u)^2 + v, a \neq 0$ beschreibt eine Parabel mit folgenden Eigenschaften:

- Offnung nach oben falls a > 0, nach unten falls a < 0
- Normalparabel falls |a| = 1