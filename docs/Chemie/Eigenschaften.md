# Eigenschaften von Molekülen, Salzen und Metalle

## Moleküle (starke Bindung)

### Eigenschaften

- Weich, verformbar
- Niedriger Schmelzpunkt
- Keine elektrische Leitfähigkeit
- Löst sich in Ähnlichem

### Zwischenmolekulare Kräfte

#### Dipol-Dipol Kräfte (stark)

Positiv geladene Teilchen und negativ geladene Teilchen von andern Molekülen werden voneinander angezogen.

!!! tip "Beispiel"
	Zwischen zwei **Chlorwasserstoffmoleküle** oder **Bromwasserstoffmoleküle**

#### Wasserstoffbrücken (am stärksten)

H-Brücken bilden sich zwischen stark positiv polarisierten H-Atomen und den freien Elektronenpaaren von N-, O- und F-Atomen

!!! tip "Beispiel"
	NH~3~ / C~2~H~5~OH

#### Van-der-Waals-Kräfte (schwach)

Van-der-Waals-Kräfte sind Anziehungskräfte zwischen spontanen Dipole aufgrund unsymmetrischer Ladungsverteilung.

!!! tip "Beispiel"
	Cl~2~ / C~2~H~6~ 

## Salze (stärkste Bindung)

### Eigenschaften

- **Hohe Schmelzpunkte**

  Es brauch viel Energie um die starke Anziehung der Ionen zu überwinden.

- ** Spröde**

  Wenn das Gitter durch Druckeinwirkung verschoben wird stossen sich gleiche Ladung ab.

- ** Elektrisch leitfähig **

  Im flüssigen Zustand sind Salze elektrisch Leitfähig

- ** Meistens Wasserlöslich**

  Wassermolekühle reissen die Ionen aus dem Gitter.

### Beispiele

!!! tip "Beispiel"
	Lithiumnitrid / Calciumnitrat

## Metalle (schwächste Bindung)

Die Atomrümpfe verlieren ihre Valenzelektronen. Die Elektronen sind frei beweglich und nichtmehr einem Atomkern zugeordnet.

### Eigenschaften

- **Hohe Siedepunkte und Schmelzpunkte**

  Starke Anziehung zwischen Elektronen und Metallatomrümpfen deshalb ist viel Energie nötig um diese zu überwinden.

- ** Elektrische Leitfähigkeit**

  Frei beweglichen Elektronen fliessen vom Minus- zum Pluspol. Sie sind Negativ geladen und werden deshalb vom Pluspool angezogen.

- ** Verformbarkeit:**

  Das Elektronengas verschiebt sich auch, wenn der Atomrumpf durch Druckeinwirkung verschoben wird.

!!! tip "Beispiel"
	CuZu / CuSu

