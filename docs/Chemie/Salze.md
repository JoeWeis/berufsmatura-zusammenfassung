# Salze

Salz sind Verbindungen die aus negativ geladen Teilchen, auch Anieonen genannt, und positiv geladen Teilchen, auch Kationen genannt, besteht.

## Schrittanleitung Kreuzregel

- [ ] Man definiere die Reaktionsgleichung mit der Lewisformel.
- [ ] Man bestimme die Ionenladung 
- [ ] Danach wird das Verhältnis im Ionengitter abgelesen.
- [ ] Zum Schluss leite man die Indices übers Kreuz ab.

## Namen

### Salze aus einatomigen Ionen

| Metallion | Nichtmetallion | id (binäre Moleküle) | Formel |
| --------- | -------------- | -------------------- | ------ |
| Natrium   | Chlor          | Natriumchlor**id**   | NaCl   |
| Natrium   | Brom           | Natriumbrom**id**    | NaBr   |



#### Anionnamen

| Formel | Name   |
| ------ | ------ |
| O^2-^  | oxid   |
| S^2-^  | sulfid |
| N^3-^  | nitrid |

### Salze aus mehratomigen Ionen (Molekülionen)

#### Kationennamen

Ammonium --> NH~4~^+^

#### Anionnamen

| Formel    | Name     |
| --------- | -------- |
| HO^-^     | Hydroxid |
| NO~3~^-^  | Nitrat   |
| SO~4~^2-^ | Sulfat   |

!!! warning "Wichtig!"
	Bei Salzen aus Nebengruppen sind mehrere unterschiedliche Ionenladungen möglich.

## Beispiele

| Kation   | Anion     | Verhältnis   | Name             |
| -------- | --------- | ------------ | ---------------- |
| Cu^+^    | O^2-^     | Cu~2~O       | Kupfer(I)-oxid   |
| Cu^2+^   | SO~4~^2-^ | CuSO~4~      | Kuper(II)-sulfat |
| NH~4~^+^ | NO~3~^-^  | NH~4~NO~3~   | Ammoniumnitrat   |
| Li^+^    | N^3-^     | Li~3~N       | Lithiumnitrid    |
| Ca^2+^   | NO~3~^-^  | Ca(NO~3~)~2~ | Calciumnitrat    |