# Chemische Reaktionen

## Grundlagen

**Koefizienten --> Ausgleich**

*Indices --> Bestummen durch die Anzahl freien Bindungen*

CH*~4~* + **2**H*~2~*O --> CO*~2~* + **4**H*~2~*

### HONClBrIF

Dies Atome treten paarweise auf.

| Formal | Name        |
| ------ | ----------- |
| H~2~   | Wasserstoff |
| O~2~   | Sauerstoff  |
| N~2~   | Stickstoff  |
| Cl~2~  | Chlor       |
| Br~2~  | Brom        |
| I~2~   | Iod         |
| F~2~   | Fluor       |

### Salzbildungsreaktion

**2**Na + Cl*~2~*-->**2**NaCl

### Säure-Base-Reaktion

NH*~3~*+H*~2~*O -->NH*~4~*^+^+OH^-^

### Verbrennungsreaktion

CH*~4~* + **2**O*~2~*-->CO*~2~* + **2**H*~2~*O

## Mengenverhältnisse

| CH*~4~* | 2O*~2~*   | CO*~2~* | 2H*~2~*O  |
| ------- | --------- | ------- | --------- |
| 1 mol   | **2** mol | 1 mol   | **2** mol |
| 24 L    | 48 L      | 24 L    | 24 L      |
| 1000 g  | 4000 g    | 2750 g  | 2250 g    |

### Volumenangaben

Aus 1 mol Methan entsteht 2 mol HH*~2~*O (1:2)

1 mol = 24 L --> **2** mol = 48 L

### Massenangaben

H = 1 g/mol; C=12 g/mol; O=16 g/mol

CH*~4~* --> 12 g + 4 x 1 g = 16 g/mol

1 kg CH*~4~*  = \frac{1000 g}{16 g/mol}$ = 62.5 mol

2 x 62.5 mol = 125 mol H*~2~*O

H*~2~*O --> 16 g + 2 x 1 g = 18 g/mol

125 mol x 18 g/mol = 2250g H*~2~*O

!!! tip "Tipp"
	Das gleiche Prinzip kann auch auf die anderen Edukte und Produkte angewendet werden.