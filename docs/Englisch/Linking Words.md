# Linking Words

## Addition (Zusatzinformationen geben)

| also             | ebenso          |
| ---------------- | --------------- |
| and (then)       | und (dann)      |
| further(more)    | außerdem        |
| In fact          | tatsächlich     |
| in addition (to) | zusätzlich (zu) |
| too              | auch            |
| equally          | gleichermaßen   |
| indeed           | tatsächlich     |
| moreover         | außerdem        |
| what is  more    | außerdem        |

## Comparison (Vergleich)

| compored  with     | verglichen mit   |
| ------------------ | ---------------- |
| let's compare      | vergleichen  wir |
| similarly          | gleichermaßen    |
| in comparison with | im Vergleich     |
| likewise           | zu gleichermaßen |
| in the same way    | gleichermaßen    |

## Contrast (Gegenüberstellung)

| but               | aber               |
| ----------------- | ------------------ |
| in contrast to    | im Gegensatz zu    |
| naturally         | natürlich          |
| olthough          | obwohl             |
| of course         | natürlich          |
| an the one hand   | einerseits         |
| still             | immer noch         |
| however           | wie auch immer     |
| instead  (of)     | anstelle  (von)    |
| nevertheless      | trotzdem,  dennoch |
| even though       | obwohl             |
| on the contrary   | im Gegenteil       |
| on the other hand | andererseits       |
| whereas           | wohingegen         |

## Enumeration (Aufzählung) 

| ftrst(ly)          | erstens      |
| ------------------ | ------------ |
| in the ftrst place | als erstes   |
| last               | schließlich  |
| second(ly)         | zweitens     |
| next               | als Nächstes |
| to end with        | abschließend |

## Giving examples (Beispiele geben) 

| for example     | zum Beispiel   |
| --------------- | -------------- |
| such as         | so wie         |
| for instance    | zum Beispiel   |
| as  evidence of | als Beweis für |

## Summary (Zusammenfassung)

| all in all    | alles in allem  |
| ------------- | --------------- |
| to sum up     | zusammenfassend |
| in brief      | kurz gefasst    |
| on the whole  | allesir. allem  |
| in conclusion | zusammenfassend |
| in short      | kurz  gefasst   |

## Time (Zeitausdrücke) 

| after (a while)    | nach (einer Weile)  |
| ------------------ | ------------------- |
| At the same time   | Gleichzeitig        |
| next               | als nächstes        |
| before (that time) | vor (dieser Zeit)   |
| so far             | bis jetzt           |
| afterwards         | danch               |
| meanwhile          | in der Zwischenzeit |
| Last               | zum Schluss         |
| then               | dann                |
| up to (then)       | Bis  (dann)         |

## Result (Ergebnis) 

| as a result       | als Ergebnis      |
| ----------------- | ----------------- |
| forthat reason    | aus diesem Grund  |
| hence             | daher             |
| thus              | daher             |
| consequently      | folglich          |
| that's the reason | das ist der Grund |
| therefore         | daher             |
| so                | daher             |