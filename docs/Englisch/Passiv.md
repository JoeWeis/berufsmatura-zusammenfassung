# Passiv

Wird ein Satz ins passiv gesetzt. Wird das Objekt an erste Stelle gesetzt. 

**active:** 

Many young people around the world like sci fi movies.

**passive:** 

Sci fi movies are liked by many young people around the world

Der passive Satz wird immer mit **be +** **past participle** gebildet.

## Simple Present

be + (not) + past participle

Harry Potter **is** **liked** by many young people 

## Present Continuous

be + (not) + being + past participle

Harry Potter **is being** **liked** by many young people 

## Present Perfect

have + (not) + been + past participle 

Harry Potter **has been** **liked** by many young people 

## Simple Past

was + (not) + past participle

Harry Potter **was** **liked** by many young people 

## Past Continuous

was + (not) + being + past participle

Harry Potter **was being** **liked** by many young people  

## Past Perfect

have + (not) + been + past participle

Harry Potter **has been** **liked** by many young people 

## will Future

will + (not) + be + past participle

Harry Potter **will be** **liked** by many young people 