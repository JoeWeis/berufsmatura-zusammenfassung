# Adjective

## Comparatives

Die "comparatives form" wird verwendet um zwei Personen oder Dinge zu vergleichen.

### Regel

Einsilbige Adjektive: *Adjektiv* + **-er**

short -> short**er**

Mehrsilbige Adjektive: **more** *Adjektiv* 

*ancient -> more ancient*

**Manche** Zweisilbige Adjektive: *Adjektiv* + **-er** oder **more** *Adjektiv* 

*gentle -> gentl***er***/***more** *gentle*

 

### Ausnahmen!

Adjektive die auf **–y** enden werden zu –**ier**

happy -> happier

 

## Superlative

Das Superlativ wird verwendet um Dinge oder Personen mit allem in dieser Gruppe zu vergleichen. 

### Regel

**Meisten** Zwei- und Einsilbige: *Adjektiv* + **-est**

shortest

Mehrsilbige Adjektive: **most** *Adjektiv* 

**most** beautiful