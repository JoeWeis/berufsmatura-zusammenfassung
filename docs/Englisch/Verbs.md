# Verbs and Tempes

## Hilfsverben 

| person    | do   | do not  | be   | was  | have | had  |
| --------- | ---- | ------- | ---- | ---- | ---- | ---- |
| I         | do   | don't   | am   | was  | have | had  |
| you       | do   | don't   | are  | were | have | had  |
| he/she/it | does | doesn't | is   | was  | has  | had  |
| we        | do   | don't   | are  | were | have | had  |
| you       | do   | don't   | are  | were | have | had  |
| they      | do   | don't   | are  | were | have | had  |

## Simple Present 

### Regel 

#### Indikativ 

Normales Verb bei he/she/it wird dem Verb ein "s" angefügt. 

#### Frage 

do/does + person + verb 

#### Negativ 

person + do not + verb 

### Verwendung 

- Dinge die immer, generell wahr sind 
- Dinge die normal sind oder immer wieder passieren 

## Present Continuous 

### Regel 

#### Indikativ 

person + be + verb-ing 

#### Frage 

be + person verb-ing 

#### Negativ 

person + be not + verb-ing 

### Verwendung 

- Dinge die geschehen während man spricht 
- Dinge die vor oder nach dem man spricht geschehen 
- Dinge die sich ändern 

## Present Perfect 

### Regel 

#### Indikativ 

person + have + past participle 

#### Frage 

Have + person+ past participle 

#### Negativ 

person + have not + past participle  

### Verwendung 

- Dinge die bis jetzt gültig waren 
- Lebenserfahrungen die man bis jetzt nicht gemacht hat. 
- Dinge die gerade geschahen  

## Simple Past 

### Regel 

#### Indikativ 

person verb-ed 

#### Frage 

did + person + verb 

#### Negativ 

person + did not + verb 

### Verwendung 

- Hauptereignisse einer Geschichte 

## Past Continuous 

### Regel 

#### Indikativ 

person + was / were + verb-ing 

#### Frage 

was / were + person + verb-ing 

#### Negativ 

person + was not / were not + verb-ing 

### Verwendung 

- Hintergrund Informationen 
- Kurze Einschubsätze zur Erklärung  

## Past Perfect 

#### Regel 

#### Indikativ 

person + had + past participle 

#### Frage 

had + person + past participle 

#### Negativ 

person + had not + past participle 

### Verwendung 

- Dinge die vor einem Hauptereignis geschahen 

 

## will Future 

### Regel 

#### Indikativ 

person + will + verb 

#### Frage 

will person + verb 

#### Negativ 

person + will not (won't) + verb 

### Verwendung 

- Vermutungen oder vorhersagen 
- Dinge die man irgendwann tun möchte 

##  going to Future 

### Regel 

#### Indikativ 

person + be + going to + verb 

#### Frage 

be + person + going to + verb 

#### Negativ 

person + be not + going to + verb 

### Verwendung 

- Dinge die geplant sind 
- Dinge die garantiert eintreffen werden 

 

 