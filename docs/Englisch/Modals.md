# Modals 

| Modal                                          | Function                                                     |
| ---------------------------------------------- | ------------------------------------------------------------ |
| can could                                      | Ability  (=tobe able to, manage to) Possibility              |
| must   ---------------   mustn't/ needn't      | Obligation   ---------------                                 |
| will / would   can /  could   may              | Prohibition/obligation  NOT to do something. lt's not necessary. |
| should/ shouldn't      ought to/ ought not  to | Giving  advice /suggestion Moral obligation           Advice |
| may /might must /will could                    | Probability/Possibility  Certainty                           |
| shall                                          | Offer                                                        |

## Wichtig

Some modal verbs need substitutes when used in the past or in the future. 

You **mustn't** touch this painting. 

- Past: You **weren't     allowed** to touch     the painting. 

You **must** sign in with a password. 

-  Past: You **had to** sign in with a     password. 

She **needn't** buy me a present. 

- Past: She **didn't have** to buy me a present. 

I **can't** come to the party.

-  Future: I **won't be able** to come. 

 

## Ersatzformen

Modals kommen bis auf can nur im present simple vor. In anderen Zeitstufen muss man Ersatzformen mit gleicher Bedeutung benutzen. 

![](images/Englisch/UR_En_217_Modalverben_Ersatzformen.gif)