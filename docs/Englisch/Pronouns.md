# Pronouns

## Defining relarive clauses

Es handelt sich um ein "Defining relarive clauses" ,wenn im Satz der Gegenstand oder die Person genau identifiziert wird.

- It's the gadget that is used     for opening cans.

 

| People                                      | who          |
| ------------------------------------------- | ------------ |
| Things                                      | which,  that |
| Besitztümer und       familiäre Beziehungen | whose        |

 

!!! warning "Ausnahme!"
	Man kann das Pronomen weglassen, wenn es das Objekt des Satzes beschreibt.
	- The first car **(that)** i had was a ten-year-old Volkswagen.

## Non defining relarive clauses

Es handelt sich um ein "Defining relarive clauses" ,wenn der Satz nur Zusatzinformationen zum Gegenstand oder der Person enthält.

- Nicolaus Otto, who died in     1891, was the inventor of the petrol engine

 

| People                                      | who   |
| ------------------------------------------- | ----- |
| Things                                      | which |
| Besitztümer und       familiäre Beziehungen | Whose |

 

!!! tip "Wichtig!"
	Bei "Non defining relarive clauses" wird ein **Komma (,)** verwendet.