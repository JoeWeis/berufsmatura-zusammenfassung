# Conditional

## Zero Conditional 

### Regel

If + present simple + , + present simple

### Usage

Eine Logische schlussfolgerung findet in der Gegenwart statt.

## First Conditional 

### Regel

If + present simple + , + will

### Usage

Eine logische Schlussfolgerung 

### Beispiel

If we hurry, we'll catch the bus 

## Secund Conditional

### Regel

If + past simple + **,** + would

### Usage

Wenn eine aussage vorstellbar ist oder eintreffen könnte.

### Beispiel

If I ate cake, I'd get fat

## Third Conditional 

### Regel

If + past perfect + **,** + would have + past participle

### Usage

 Unrealistisch oder es ist schon zu spät um es zu ändern. (Vergangenheit.)

### Beispiel

If Matthew had phoned her, Emma wouldn't have been so annoyed

## Mix Secund & Third Conditional 

### Regel

If + past perfect + **,** + would

### Usage

Oft Verwendetes Konjunktiv

### Beispiel

If you had planned things properly, you wouldn't get into a mess