# Geschwindigkeit als Vektor

| Addition von Geschwindigkeitsvektoren: Superpositionsprinzip | $𝑣⃗ = 𝑣⃗_1 + 𝑣⃗_2$ |
| ------------------------------------------------------------ | --------------- |
| Zerlegung in senkrecht zueinanderstehenden Teilbewegungen:   |                 |
| **Bewegung in der Ebene:**|                 |
| Gleichförmige Bewegung in 𝑥 − Richtung <br />Gleichförmige Bewegung in 𝑦 − Richtung |
| **Horizontaler Wurf:**                                       |                 |
| Gleichförmige Bewegung in 𝑥 − Richtung Beschleunigte Bewegung in 𝑦 − Richtung |                 |
|                                                              |                 |

