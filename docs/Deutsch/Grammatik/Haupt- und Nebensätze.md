# Haupt- und Nebensätze

## Hauptsatz

- Er kann alleine stehen
- Der kenjugierte Teil des **Prädikats steht an zweiter Stelle**

## Nebensatz

- Er kann nicht alleine stehen
- Der konjubierte Teil des **Prädikats steht am Schluss**

### Uneingeleitete Nebensätze



### Nebensätze mit Pronominaladvernien

## Satzreihe, Satzverbindung, Satzgefüge

## Zusammengezogene Sätze

Zusammengezogene Sätze beziehen sich alle auf das gleiche Subjekt, welches nur einmal aufgeliestet werden.

## Der Konjunktionsalsatz

## Der Relativsatz

## Der indirekte Fragesatz

